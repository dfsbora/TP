%This program plots a trajectory by the positions X and Y given in the file output.txt
load('output.txt');
output2 = zeros(500:2);
output2(1:500, 1:2) = output(1:500, 1:2);

figure();
subplot (2,1,1);
axis([-1 1 -1 1]);
plot(output(:,1),output(:,2));
title('Todos os pontos da trajetória da bola');
subplot (2,1,2);
axis([-1 1 -1 1]);
plot(output2(:,1), output2(:,2));
title('Primeiros 500 pontos da trajetória da bola');