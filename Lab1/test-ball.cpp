/** file: test-ball.cpp
 ** brief: Tests the bouncing ball simulation
 ** author: Andrea Vedaldi
 **/

#include "ball.h"
#include <iostream>

using namespace std;

void run(Simulation & s, double dt)
{
  for (int i = 0 ; i < 100 ; ++i)
    {
      s.step(dt) ;
      s.display() ;
    }
}
int main(int argc, char** argv)
{
  Ball ball ;
  double test;
  //Function to set position
  ball.set();

  const double dt = 1.0/100 ;
  // for (int i = 0 ; i < 10000 ; ++i) {
  //   ball.step(dt) ;
  //   ball.display() ;
  // }
  run (ball, dt);
  
  return 0 ;
}
