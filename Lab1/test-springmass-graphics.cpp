/** file: test-springmass-graphics.cpp
 ** brief: Tests the spring mass simulation with graphics
 ** author: Andrea Vedaldi
 **/

#include "graphics.h"
#include "springmass.h"

#include <iostream>
#include <sstream>
#include <iomanip>

/* ---------------------------------------------------------------- */
class SpringMassDrawable : public SpringMass, public Drawable
/* ---------------------------------------------------------------- */
{

private:
  Figure figure ;

public:
  SpringMassDrawable()
  : figure("Spring Mass")
  {
    figure.addDrawable(this) ;
  }

  void draw()
  {
  	for (int i=0; i < mass_vector.size(); i++)
  	{
		figure.drawCircle(mass_vector[i].getPosition().x , mass_vector[i].getPosition().y, mass_vector[i].getRadius()) ;	
  	}

  	for (int i=0; i < spring_vector.size(); i++)
  	{
		figure.drawLine( spring_vector[i].getMass1()->getPosition().x , spring_vector[i].getMass1()->getPosition().y , spring_vector[i].getMass2()->getPosition().x , spring_vector[i].getMass2()->getPosition().y, 0.5) ;	
  	}   
  }

  void display() {
    figure.update() ;
  }

/* DONE: TYPE YOUR CODE HERE */

} ;

int main(int argc, char** argv)
{
  glutInit(&argc,argv) ;
  const double mass = 0.05 ;
  const double radius = 0.02 ;
  const double naturalLength = 0.95 ;
  const double stiffness = 0.8; //Stiffness adicionada
  const double damping = 0.8; //Damping adicionado

  SpringMassDrawable springmass ;


  Mass m1(Vector2(-.3,-0.2), Vector2(), mass, radius) ;
  Mass m2(Vector2(+.5,0), Vector2(), mass, radius) ;
  springmass.addMass(&m1);
  springmass.addMass(&m2);


  Spring spring(&m1,&m2,naturalLength,stiffness, damping);
  springmass.addSpring(&spring);

  const double dt = 1/60.0 ;
  run(&springmass, 1/120.0) ;
  /* DONE: TYPE YOUR CODE HERE */
  return 0 ;
}
