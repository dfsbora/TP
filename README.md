# TP

## Softwares e versões
Nesse projeto são utilizados
- gcc 4.8.4
- MATLAB
- Ubuntu 14.04.5 LTS

## Requisitos
O projeto Spring Mass Graphics utiliza pacotes de OpenGL, para instalação usar a linha de comando:

    $ sudo apt-get install freeglut3 freeglut3-dev


# Bouncing Ball
Esse projeto simula um sistema dinâmico formado por uma bola submetida ao efeito da gravidade, considerando-se conservação de energia. 

## Compilação e execução
Para compilar, usar a linha de comando:

    $ make test-ball
Para execução, usar a linha de comando:    

    $ ./test-ball

## Arquivos
Os arquivos utilizados inicialmente são descritos abaixo:
- ball.h
    + Header da classe ball.
- ball.cpp
    + Implementa a classe ball.
- test-ball.cpp
    + Módulo principal. Testa a simulação da bola.
- plot_matlab.m
    + Carrega aquivo output.txt e, com as informações das posições (X,Y), plota a trajetória da bola.

## Saída gerada
A saída gerada é do formato 'X Y', apresentando a posição em x e y da bola.
Exemplo:
> 0.782 0.42971

> 0.785 0.441

> 0.788 0.45131

## Gráfico
A saída pode ser salva em um arquivo texto utilizando o comando:
    
    $ ./test-ball > output.txt
No software MATLAB, o script *plot_matlab.m* carrega o arquivo *output.txt* e gera uma figura conforme a figura abaixo. O primeiro gráfico apresenta toda a trajetória da bola. O segundo apresenta apenas os primeiros 500 pontos para um melhor entendimento da trajetória gerada.

![Alt text](https://gitlab.com/dfsbora/TP/uploads/f187460d6af1fb068657975e109467fe/grafico.jpg)




# Spring Mass
Esse projeto simula um sistema dinâmico formado por duas ou mais bolas submetidas ao efeito da gravidade e conectadas por uma ou mais molas entre si.

## Compilação e execução
Para compilar, usar a linha de comando:

    $ make test-springmass
Para execução, usar a linha de comando:    

    $ ./test-springmass

## Arquivos
Os arquivos utilizados inicialmente são descritos abaixo:
- springmass.h
    + Header da simulação de SpringMass. Define clases Mass, Spring e SpringMass
- springmass.cpp
    + Implementação da simulação de SpringMass. Implementa as classes Mass, Spring e SpringMass.
- test-springmass.cpp
    + Módulo principal. Testa a simulação da bola.
- plot_matlabspringmass.m
    + C

## Saída gerada
As saída consiste na impressão dos valores da posição das massas do sistema e, também, do comprimento das molas do sistema para cada nova iteração.
No exemplo a seguir há duas massas e uma única mola.

> (-0.3,-0.6356)

> (0.5,-0.8649)

> $0.824621

> (-0.3,-0.5969)

> (0.5,-0.9216)

> $0.824621

> (-0.3,-0.56)

> (0.5,-0.9216)

> $0.824621

## Gráfico
A saída pode ser salva em um arquivo texto utilizando o comando:
    
    $ ./test-ball > output_springmass.txt
    
No software MATLAB, o script *plot_matlabspringmass.m* carrega o arquivo *output_springmass.txt* e gera uma figura conforme a figura abaixo. O primeiro gráfico apresenta toda a trajetória da bola. O segundo apresenta apenas os primeiros 500 pontos para um melhor entendimento da trajetória gerada.

![Alt text]()





# Spring Mass Graphics
Esse projeto simula graficamente o sistema modelado anteriormente em SpringMass

## Compilação e execução
Para compilar, usar a linha de comando:

    $ make test-springmass-graphics
Para execução, usar a linha de comando:    

    $ ./test-springmass-graphics

## Arquivos
Os novos arquivos utilizados nesse projeto são descritos abaixo:
- graphics.cpp
    + Implementação das classes responsáveis pelo desenho na tela de círculos, linhas e strings.
- test-ball-graphics.cpp
    + Implementa a simulação gráfica do projeto 1 - Bouncing Ball
- test-springmass-graphics.cpp
    + Implementa a simulação gráfica do projeto 2 - Spring Mass



## Gráfico
A saída obtida é apresentada na figura abaixo.

![Alt text](https://gitlab.com/dfsbora/TP/uploads/95ecae3f7787a67ad1df0a618ff0a0fa/imagem.png)

